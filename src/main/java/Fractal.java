import org.joml.Vector3d;
import org.joml.Vector4d;
import org.lwjgl.Sys;

import java.util.ArrayList;
import java.util.List;


public class Fractal {

    public static Vector3d spherical(double x, double y, double z) {
        double r = Math.sqrt(x * x + y * y + z * z);
        double theta = Math.atan2(Math.sqrt(x * x + y * y), z);
        double phi = Math.atan2(y, x);
        return new Vector3d(r, theta, phi);
    }

    public static List<Vector4d> getFractal(double range, double range1, double range2, double precision, double n) {
        List<Vector4d> fractal = new ArrayList<>();
        for (double x = range1; x < range2; x = x + precision) {
            for (double y = -range; y < range; y = y + precision) {
                boolean edge = false;
                for (double z = -range; z < range; z = z + precision) {
                    Vector3d zeta = new Vector3d(0, 0, 0);
                    int maxIter = 10;
                    int iter = 0;
                    while (true) {
                        Vector3d c = spherical(zeta.x, zeta.y, zeta.z);
                        double sinXn = Math.pow(c.x, n);
                        double sinYn = Math.sin(c.y * n);
                        double cosZn = Math.cos(c.z * n);
                        double sinZn = Math.sin(c.z * n);
                        double newX = sinXn * sinYn * cosZn;
                        double newY = sinXn * sinYn * sinZn;
                        double newZ = sinXn * sinYn;
                        zeta.x = newX + x;
                        zeta.y = newY + y;
                        zeta.z = newZ + z;
                        iter++;
                        if (c.x > 16){
                            if (edge) edge = false;
                            break;
                        }
                        if (iter > maxIter) {
                            if (!edge){
                                edge = true;
                                Vector3d vector3d = new Vector3d(x,y,z);
                                double dist = vector3d.distance(0,0,0);
                                fractal.add(new Vector4d(x, y, z, dist ));
                            }
                            break;
                        }
                    }
                }
            }
        }
        System.out.println("Number of points in fractal in range" + range1 + " " + range2 + " are " + fractal.size());
        return fractal;
    }


}
