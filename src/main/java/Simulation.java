
import java.io.BufferedReader;
import java.io.IOException;

import java.io.InputStreamReader;
import java.lang.Math;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.joml.*;
import org.lwjgl.Sys;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GLUtil;
import org.lwjgl.system.MemoryUtil;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.NULL;


public class Simulation {

    private static double scaler = 1.0;
    private static double rotate_x = 0.0;
    private static double rotate_y = 0.0;
    private static double rotate_z = 0.0;
    private static boolean redraw = false;

    private static GLFWKeyCallback keyCallback = new GLFWKeyCallback() {

        @Override
        public void invoke(long window, int key, int scancode, int action, int mods) {
            if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
                glfwSetWindowShouldClose(window, true);
            }
            if (key == GLFW_KEY_UP){
                scaler = scaler + 0.025;
            }
            if (key == GLFW_KEY_DOWN){
                scaler = scaler - 0.025;
            }
            if (key == GLFW_KEY_Q){
                rotate_x = rotate_x + 0.05;
            }
            if (key == GLFW_KEY_A){
                rotate_x = rotate_x - 0.05;
            }
            if (key == GLFW_KEY_W){
                rotate_y = rotate_y + 0.05;
            }
            if (key == GLFW_KEY_S){
                rotate_y = rotate_y - 0.05;
            }
            if (key == GLFW_KEY_E){
                rotate_z = rotate_z + 0.05;
            }
            if (key == GLFW_KEY_D){
                rotate_z = rotate_z - 0.05;
            }
            if (key == GLFW_KEY_N && action == GLFW_PRESS){
                redraw = true;
            }
        }
    };

    public static Vector3d spherical(double x, double y, double z){
        double r = Math.sqrt(x*x + y*y + z*z);
        double theta = Math.atan2(Math.sqrt(x*x + y*y), z);
        double phi = Math.atan2(y,x);
        return new Vector3d(r, theta, phi);
    }

    public static List<Vector4d> calculate_fractal() throws IOException {

        //get input
        System.out.println("Type in the value of n and press ENTER: ");
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(System.in));

        // Reading data using readLine
        int n = Integer.valueOf(reader.readLine());

        System.out.println("Creating 8 threads to calculate the whole fractal... ");
        // Create an array of 8 threads
        Thread[] threads = new Thread[8];
        List<Vector4d> fractal = new ArrayList<>();

        double precision = 0.005;

        // Initialize the threads
        for (int i = 0; i < 8; i++) {
            final int param = i; // final is required because it is accessed from within the anonymous class below

            threads[i] = new Thread(
                    new Runnable() {
                        @Override
                        public void run() {
                            if (param == 0){
                                fractal.addAll(Fractal.getFractal(0.85,-0.85, -0.7, precision, n));
                                System.out.println("Thread " + param+ " finished");
                                return;
                            }
                            if (param == 1){
                                fractal.addAll(Fractal.getFractal(0.85, -0.7, -0.5, precision, n));
                                System.out.println("Thread " + param+ " finished");
                                return;
                            }
                            if (param == 2){
                                fractal.addAll(Fractal.getFractal(0.85,-0.5, -0.3, precision, n));
                                System.out.println("Thread " + param+ " finished");
                                return;
                            }
                            if (param == 3){
                                fractal.addAll(Fractal.getFractal(0.85,-0.3, 0, precision, n));
                                System.out.println("Thread " + param+ " finished");
                                return;
                            }
                            if (param == 4){
                                fractal.addAll(Fractal.getFractal(0.85,0, 0.3, precision, n));
                                System.out.println("Thread " + param+ " finished");
                                return;
                            }
                            if (param == 5){
                                fractal.addAll(Fractal.getFractal(0.85,0.3, 0.5, precision, n));
                                System.out.println("Thread " + param+ " finished");
                                return;
                            }
                            if (param == 6){
                                fractal.addAll(Fractal.getFractal(0.85,0.5, 0.7,precision, n));
                                System.out.println("Thread " + param+ " finished");
                                return;
                            }
                            if (param == 7){
                                fractal.addAll(Fractal.getFractal(0.85, 0.7, 0.85, precision,n ));
                                System.out.println("Thread " + param+ " finished");
                                return;
                            }
                        }
                    });
        }

        // Start the threads
        Arrays.stream(threads).forEach(Thread::start);

        // Wait for the threads to finish
        Arrays.stream(threads).forEach(t -> {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        return fractal;
    }


    public static void main(String[] args) throws IOException, InterruptedException {

        long window;
        /* Initialize GLFW */
        if (!glfwInit()) {
            throw new IllegalStateException("Unable to initialize GLFW");
        }
        /* Create window */
        window = glfwCreateWindow(1280, 720, "Simple example", NULL, NULL);
        if (window == NULL) {
            glfwTerminate();
            throw new RuntimeException("Failed to create the GLFW window");
        }
        /* Center the window on screen */
        GLFWVidMode vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        glfwSetWindowPos(window,
                (vidMode.width() - 1280) / 2,
                (vidMode.height() - 720) / 2
        );
        /* Create OpenGL context */
        glfwMakeContextCurrent(window);
        GL.createCapabilities();
        /* Enable vertical synchronization */
        glfwSwapInterval(1);
        /* Set the key callback */
        glfwSetKeyCallback(window, keyCallback);
        /* Declare buffers for using inside the loop */
        IntBuffer width = MemoryUtil.memAllocInt(1);
        IntBuffer height = MemoryUtil.memAllocInt(1);


        List<Vector4d> fractal = calculate_fractal();

        /* Loop until window gets closed */
        while (!glfwWindowShouldClose(window)) {
            if (redraw) {
                fractal = calculate_fractal();
                redraw = false;
            }
            float ratio;
            /* Animate the scene */
            /* Get width and height to calcualte the ratio */
            glfwGetFramebufferSize(window, width, height);
            ratio = width.get() / (float) height.get();
            /* Rewind buffers for next get */
            width.rewind();
            height.rewind();
            /* Set viewport and clear screen */
            glViewport(0, 0, width.get(), height.get());
            glClear(GL_COLOR_BUFFER_BIT);
            /* Set ortographic projection */
            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            double zoomlevel = 0.5;
            glOrtho( -ratio * 0.5f * zoomlevel, ratio * 0.5f * zoomlevel, -ratio  * 0.5f * zoomlevel, ratio  * 0.5f * zoomlevel, 100, -100 );

            glMatrixMode(GL_MODELVIEW);

            glLoadIdentity();
            glRotated( 100,  rotate_x ,  rotate_y, rotate_z );
            glScaled(scaler, scaler, scaler);

            glBegin(GL_POINTS);
            double partticleSize = 0.0005;
            for (Vector4d vector4d : fractal) {
                glColor4d(1, vector4d.w*0.6, vector4d.w*0.4, vector4d.w*0.3);
                glVertex3d(vector4d.x, vector4d.y, vector4d.z);
            }
            glEnd();




            //FINISH ONE ITERATION
            /* Swap buffers and poll Events */
            glfwSwapBuffers(window);
            glfwPollEvents();
            /* Flip buffers for next loop */
            width.flip();
            height.flip();

        }
        /* Free buffers */
        MemoryUtil.memFree(width);
        MemoryUtil.memFree(height);
        /* Release window and its callbacks */
        glfwDestroyWindow(window);
        keyCallback.free();
        /* Terminate GLFW and release the error callback */
        glfwTerminate();
    }
}